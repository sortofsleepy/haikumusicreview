(ns haikumusicreview.dev
  (:require [figwheel.client]
            [haikumusicreview.core]))

(defn -main []
  (figwheel.client/start)
  (haikumusicreview.core/-main))

(set! *main-cli-fn* -main)
