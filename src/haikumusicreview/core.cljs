(ns haikumusicreview.core
  (:require [cljs.nodejs :as nodejs]
            [twitter.core :as twitter]
            [echonest.core :as echonest]
            [haikumusicreview.wordtest :as wordtest]
            [processor.core :as processor]))

(nodejs/enable-util-print!)

(def fs (nodejs/require "fs"))

;;remember this is relative to out.dev
(def __dirname (js* "__dirname"))

;; require express 
(def express (nodejs/require "express"))
(def app (express ()))


(defn -main []
 (let [review (processor/processReview  (clj->js
  {:url "http://www.billboard.com/articles/news/6289405/taylor-swift-1989-album-review"}
  ) (fn [wordlist]
     (wordtest/buildHaiku wordlist)
  ))]
  ())
  (doto app

 
    (.get "/" (fn [req res]
      (.send res "nothing to see here")))
      
    ;; processes a artist's reviews  
    (.get "/process/:artist" (fn [req res]
      (echonest/getReviews (.-params.artist req) (fn [contents]
        (let [reviews (.-response.reviews contents)]
          (processor/processReviews reviews))
         
       
        ))))
      
        
      
      (.listen 3000)))

(set! *main-cli-fn* -main)
