
(ns haikumusicreview.wordtest
  (:require [cljs.nodejs :as nodejs]
       		[processor.core :as processor]
         ))


;; a map of positive words to keep a eye out for
(def positivewords {	
	:good 1 
	:delightful 3 
	:happy 2
	:joyus 2
	:stupendous 3
	:hilariously 5
	:best 2
	:simpatico 4
	:rich 1
	:balls 1
	:gripping 2
	:rare 1
	:blessed 1
	:alluring 3
	:admire 3
	:beautifully 4
	:expertly 3
	:thriving 2
	:confidence 3
	})

;; a map of negative words to watch out for
(def negativewords {
	:bad 1
	:sad 1
	:disheartening 4
	:awful 2
	:hilariously 5
	:grandiose 3
	:extremes 2
	:worrying 3
	:abandoned 3
	:awkward 2
	:clunky 2
	:balls 1
	:fails 1
	:older 2
	:tired 2
	:weakest 2
	:barely 2
	:thin 1
	:pinched 1
	:grief 1
	:turmoil 3
	:distracted 3
	});
	
;; sorts through the lists, removing any duplicates as well as starting to weight the words
;; Steps 
;; 1. Count up the positive vs negative words.
;; 2. More positive words than negative words results in positive -> positive -> negative stanzas
;; 3. More negative words than positive words results in negative -> negative -> positive stanzas
;; 4. A tie results in alternating stanzas, the starting type of which is determined by the type of the first word found
;; 
;; Forming Stanzas
;; Look through list. after the first word, look for the next word that could complete the line. 
;; TODO make sure there are plently of words so that the risk of running out of words to complete the haiku are at a minium
;; Remember - 0 is a positive word, 1 for type is representative of a negative word 
(defn buildHaiku [wordlist]
	(let [haiku ""]
		(let [negativelist #js []]
			(let [positivelist #js []]
					;; loop through the list
					(dotimes [i (count wordlist)] 
						(let [word (get wordlist i)]
							(let [wordType (aget word "type")]
								(if (== 0 wordType)
									(.push positivelist (aget word "word"))										
									(.push negativelist (aget word "word"))				
									 )
								);; end wordtype check
						
							);; end word declaration
						);; end word loop statement
						
						;; check for number of positive words vs negative words
						(if (> positiveWords negativeWords) 
							(println "positive") 
				
							;;build a negative stanazed haiku
							(println "negative")
				
						);;end if
						
					 );; end positivelist
				 );; end negativelist
			 );; end haiku let
	);; end function
		
	