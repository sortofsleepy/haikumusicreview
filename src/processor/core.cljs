(ns processor.core
  (:require [cljs.nodejs :as nodejs]
  			[cuerdas.core :as str]
  			[echonest.core :as echonest]))
			  
;; cheerio for picking apart the DOM of a review site
(def cheerio (nodejs/require "cheerio"))

(def request (nodejs/require "request"))

;; a map of positive words to keep a eye out for
(def positivewords {	
	:good 1 
	:delightful 3 
	:happy 2
	:joyus 2
	:stupendous 3
	:hilariously 5
	:best 2
	:simpatico 4
	:rich 1
	:balls 1
	:gripping 2
	:rare 1
	:blessed 1
	:alluring 3
	:admire 3
	:beautifully 4
	:expertly 3
	:thriving 2
	})

;; a map of negative words to watch out for
(def negativewords {
	:bad 1
	:sad 1
	:breaking 2
	:disheartening 4
	:awful 2
	:hilariously 5
	:grandiose 3
	:extremes 2
	:worrying 3
	:abandoned 3
	:awkward 2
	:clunky 2
	:balls 1
	:fails 1
	:older 2
	:tired 2
	:weakest 2
	:barely 2
	:thin 1
	:pinched 1
	:grief 1
	:turmoil 3
	:Nashville 4
	});
	
;; sorts through the lists, removing any duplicates as well as starting to weight the words
;; Steps 
;; 1. Count up the positive vs negative words.
;; 2. More positive words than negative words results in positive -> positive -> negative stanzas
;; 3. More negative words than positive words results in negative -> negative -> positive stanzas
;; 4. A tie results in alternating stanzas, the starting type of which is determined by the type of the first word found
;; 
;; Forming Stanzas
;; Look through list. after the first word, look for the next word that could complete the line. 
;; TODO make sure there are plently of words so that the risk of running out of words to complete the haiku are at a minium
;; Remember - 0 is a positive word, 1 for type is representative of a negative word 
(defn buildHaiku [wordlist]
	(let [haiku ""]
		(let [positiveWords 0]
		(let [negativeWords 0]
		
			;; loop through the list
			(dotimes [i (count wordlist)] 
				(let [word (get wordlist i)]
					(let [wordType (aget word "type")]
						(if (== 0 wordType)
							(+ 1 positiveWords)
							(+ 1 negativeWords))
						);; end wordtype check
						
				);; end word declaration
				);; end word loop statement
	
			;; check for number of positive words vs negative words
			(if (> positiveWords negativeWords) 
				(println "positive") 
				(println "negative"))
	
		))
		
		));;end function
		
	
	
;; compares tag contents to mapping of words. 
;; returns a array of found positive and negative wrods
(defn compareWords [tag]
	;; strip html tags
	(let [clean (str/strip-tags tag)]
		(let [foundWords #js []]
		
				;; loop through positive words to try and find a match
				(doseq [[k v] positivewords]
					;; run regular expression match comparing word in dictionary to cleaned tag
					;; if not null, then we have a match
					(let [re (.indexOf clean (.-name k))]
						(if (not= -1 re)
							(.push foundWords (clj->js {:word k :type 0 :count v}))

							()) ) ;; if nothing was found do nothing
					)
					
				;; loop thorugh negative words to try and find a match	
				(doseq [[k v] negativewords]
					;; run regular expression match comparing word in dictionary to cleaned tag
					;; if not null, then we have a match
					(let [re (.indexOf clean (.-name k))]
						(if (not= -1 re)
							(.push foundWords (clj->js {:word k :type 1 :count v}))
							()) ) ;; if nothing was found do nothing
					)	
			foundWords ) ;;return foundwords array
		))	
	
	
;; processes one review
;; processes one review
(defn processReview [review cb]
	(let [pagedata (request (.-url review) (fn [error response body]
		 (if (= error true)
		 	(js/console.log "error processing review")
			 
			 (let [finalWords #js [] ]
				 ;; if we got a response, load DOM representation into variable
			 	(let [content (.load cheerio body)]
			 		;;grab all the <p> tags
			 		(let [copyContent (content "p")]
					 	;; iterate through cheerio objects
					 	(doseq [copy (array-seq copyContent)]
						 
						 ;; parse those objects, then grab the html
						 (let [tag (.html (cheerio copy))]
						 
						 	;; run through each tag and compare with the dictionary. 
							;; Returns a list of found positive and negative words
						 	(let [words (compareWords tag)]
						
							(if (not= 0 (.-length words))
								 (.push finalWords (clj->js {:words (clj->js words) :tag tag}))
								 () )
							 ))))
							 
				)
				;; send processed review to callback
				(cb finalWords) )
				
				
				)))]))
				

;; processes a stack of reviews 
(defn processReviews [reviews]
	;; set aside a vector for compiled reviews
	(let [processedReviews #js {}]
		;; get the number of reviews
		(let [numreviews (count reviews)]
		
			;;process the first review in the list
			(let [review (rand-nth reviews)] 
					;;process the reviews in a seperate function for clarity
					(let [processedReview "test"]
					
						())
		
			))))

