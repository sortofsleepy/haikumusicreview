(ns echonest.core
  (:require [cljs.nodejs :as nodejs]))
  
 ;;query object
 (def query (nodejs/require "querystring"))

 ;; request object to pull the actual page of the review
 (def request (nodejs/require "request"))

 ;; api key 
 (def apikey "YXDWCVHES8RWV4IAH")

 ;; base url for getting reviews
 (def reviewBaseUrl "http://developer.echonest.com/api/v4/artist/reviews?")
 
 ;; base url for getting "hottness"
 (def hottnessUrl "http://developer.echonest.com/api/v4/artist/hotttnesss?api_key=YXDWCVHES8RWV4IAH&format=json")
 
 ;; gets the "hottness" of the artist as a numerical value
 (defn getHottness [artist cb]
   (let [reviewurl (str hottnessUrl "&name=" artist)]
          (request reviewurl (fn [error response body]
            (if (= error true)
              (js/console.log "Issue pinging echonest api" response)
              (cb (js/JSON.parse body))
              )))))
 
 ;; Pings echonest api for review urls for a artist
 ;; Example : (echonest/getReviews "radiohead" (fn [contents]
 ;;             (js/console.log contents)))
 (defn getReviews [artist cb]
    ;; first form querystring
    (let [querystring
            (.stringify query #js {
              :api_key apikey
              :name artist
              :format "json"
              :results 2
              :start 0})]
        ;; combine to form full echonest request url       
        (let [reviewurl (str reviewBaseUrl querystring)]
          (request reviewurl (fn [error response body]
            (if (= error true)
              (js/console.log "Issue pinging echonest api" response)
              (cb (js/JSON.parse body))
              ))))))
              
                              